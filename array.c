#include <stdio.h>
int input_array_size()
{
   int n;
   printf("Enter array size\n");
   scanf("%d",&n);
   return n;
}

void input_array(int n, int a[n])
{
   for(int i=0;i<n;i++)
   {
       printf("Enter the element a[%d]: ",i);
       scanf("%d",&a[i]);
    }
}

int sum_of_an_array(int n, int a[n])
{
    int sum=0;
    for(int i=0;i<n;i++)
    {
        sum=sum+a[i];
    }
    return sum;
}

void output(int n, int a[n], int sum)
{
    int i;
    for(i=0;i<n-1;i++)
    {
        printf("%d+",a[i]);
    }
    printf("%d=%d\n",a[i],sum);
}

int main()
{
   int n=input_array_size();
   int a[n];
   input_array(n,a);
   int sum=sum_of_an_array(n,a);
   output(n,a,sum);
   return 0;
}